package fr.polytech.polyrentech.Converter;


import fr.polytech.polyrentech.Model.User;
import fr.polytech.polyrentech.ViewModel.LoginViewModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestLoginConverter {

    @Test
    public void should_return_user_when_both_value_filled() {
        LoginViewModel lvm = new LoginViewModel();
        lvm.login().setValue("toto.dupont@test.fr");
        lvm.password().setValue("password123");

        LoginConverter lc = new LoginConverter();

        // tested method
        User user = lc.toUser(lvm);

        assertEquals("toto.dupont@test.fr", user.getLogin(), "Login from user doesn't match login of viewmodel");
        assertEquals("password123", user.getPassword(), "password from user doesn't match password of viewmodel");
    }

    @Test
    public void should_throw_illegal_argument_exception_when_login_empty() {
        LoginViewModel lvm = new LoginViewModel();
        lvm.login().setValue("toto.dupont@test.fr");
        lvm.password().setValue("");

        LoginConverter lc = new LoginConverter();

        lvm.login().setValue("");
        lvm.password().setValue("password123");

        assertThrows(IllegalArgumentException.class, () -> lc.toUser(lvm), "converter doesn't throw exception when login empty");
    }

    @Test
    public void should_throw_illegal_argument_exception_when_password_empty() {
        LoginViewModel lvm = new LoginViewModel();
        lvm.login().setValue("toto.dupont@test.fr");
        lvm.password().setValue("");

        LoginConverter lc = new LoginConverter();

        assertThrows(IllegalArgumentException.class, () -> lc.toUser(lvm), "converter doesn't throw exception when password empty");
    }

    @Test
    public void should_throw_null_pointer_exception_when_viewmodel_is_null() {
        LoginConverter lc = new LoginConverter();

        assertThrows(NullPointerException.class, () -> lc.toUser(null), "converter doesn't throw null pointer exception when viewmodel null");
    }
}
