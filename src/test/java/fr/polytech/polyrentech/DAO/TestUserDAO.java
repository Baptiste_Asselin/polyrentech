package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.jupiter.api.Assertions.*;

public class TestUserDAO {

    @BeforeAll
    public static void prepare_tests() {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("insert into user(name, surname, login, password, registration, admin) values(?,?,?,?,?,?);");
            ps.setString(1, "Dupont");
            ps.setString(2, "Toto");
            ps.setString(3, "toto.dupont@email.com");
            ps.setString(4, "password");
            ps.setString(5, "EMPTOTO");
            ps.setBoolean(6, true);
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void should_return_user_on_existing_login() {
        User testuser = UserDAO.getUser("toto.dupont@email.com");
        assertNotNull(testuser, "getUser return null but user exist in database");
        assertEquals("Dupont", testuser.getName());
        assertEquals("Toto", testuser.getSurname());
        assertEquals("toto.dupont@email.com", testuser.getLogin());
        assertEquals("password", testuser.getPassword());
        assertEquals(true, testuser.getAdmin());
    }

    @Test
    public void should_return_null_on_not_exiting_login() {
        User testuser = UserDAO.getUser("existe.pasdutout@email.com");
        assertNull(testuser);
    }

    @AfterAll
    public static void after_tests() {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("delete from user where login=?");
            ps.setString(1, "toto.dupont@email.com");
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
