package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.Material;
import fr.polytech.polyrentech.ViewModel.MaterialViewModel;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestMaterialDAO {
    @Test
    public void save_should_return_the_right_idMaterial_when_good_material(){
        Material material = new Material("Iphone" , "V1" , "IP001" , "h" ,"Le premier iphone utilisé pour les tests");
        //tested method
        int idMaterial = MaterialDAO.save(material);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("IP001");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);

    }
    @Test
    public void save_should_return_Illegal_Argument_Exception_when_material_is_already_in_database() {
        Material material = new Material("Iphone" , "V1" , "An105" , "gr" ,"Le premier iphone utilisé pour les tests");
        MaterialDAO.save(material);
        assertThrows(IllegalArgumentException.class, () -> MaterialDAO.save(material), "Ce materiel existe deja avec la meme reference dans la base de donnée");
        MaterialDAO.delete(material);
    }
    @Test
    void delete_should_remove_material_from_database(){
        Material material = new Material("TESTDELETE1" , "V1" , "TESTD" , "h" ,"Test delete");
        MaterialDAO.save(material);
        //tested method
        MaterialDAO.delete(material);
        int idMaterial = MaterialDAO.getIdMaterialFromReference(material.getReference());
        //Cette assert vérifie que le material a bien été supprimé.
        assertEquals(0 , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_right_complete_name(){
        Material material = new Material("Samsungtest1" , "A20" , "TEST1" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("Samsungtest1").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST1");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_right_complete_version(){
        Material material = new Material("Samsung" , "versiontest2" , "TEST2" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("versiontest2").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST2");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_right_complete_reference(){
        Material material = new Material("Samsung" , "A20" , "TEST3" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("TEST3").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST3");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_right_complete_materialInformation(){
        Material material = new Material("Samsung" , "A20" , "TEST4" , "h" ,"Samsung utilisé pour les tests4");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("Samsung utilisé pour les tests4").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST4");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_not_complete_name(){
        Material material = new Material("Samsungtest5" , "A20" , "TEST5" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("ungtest5").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST5");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_not_complete_version(){
        Material material = new Material("Samsung" , "versiontest6" , "TEST6" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("iontest6").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST6");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_not_complete_reference(){
        Material material = new Material("Samsung" , "A20" , "TEST7" , "h" ,"Samsung utilisé pour les tests");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("EST7").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST7");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_idMaterial_when_research_is_not_complete_materialInformation(){
        Material material = new Material("Samsung" , "A20" , "TEST8" , "h" ,"Samsung utilisé pour les tests8");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterial("les tests8").get(0);
        int rightIdMaterial = MaterialDAO.getIdMaterialFromReference("TEST8");
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterial_should_return_empty_list_when_research_is_wrong(){
        Material material = new Material("Samsung" , "A20" , "TEST9" , "h" ,"Samsung utilisé pour les tests8");
        MaterialDAO.save(material);
        //tested method
        List<Integer> idMaterials = MaterialDAO.getIdMaterial("TEST9T");
        MaterialDAO.delete(material);
        assertEquals(new LinkedList<Integer>() , idMaterials);
    }
    @Test
    public void getIdMaterialFromReference_should_return_right_idMaterial(){
        Material material = new Material("SamsungTest10" , "A20" , "TES10" , "h" ,"Samsung utilisé pour les tests8");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterialFromReference("TES10");
        int rightIdMaterial = MaterialDAO.getIdMaterial("SamsungTest10").get(0);
        MaterialDAO.delete(material);
        assertEquals(rightIdMaterial , idMaterial);
    }
    @Test
    public void getIdMaterialFromReference_should_return_0_when_no_idMaterial_found(){
        Material material = new Material("Samsung" , "A20" , "TES11" , "h" ,"Samsung utilisé pour les tests8");
        MaterialDAO.save(material);
        //tested method
        int idMaterial = MaterialDAO.getIdMaterialFromReference("ES11T");
        MaterialDAO.delete(material);
        assertEquals(0 , idMaterial);
    }
}
