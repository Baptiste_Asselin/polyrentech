package fr.polytech.polyrentech.DAO;
import fr.polytech.polyrentech.Model.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestLoginDAO {

    @BeforeAll
    public static void prepare_tests() {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("insert into user(name, surname, login, password, registration, admin) values(?,?,?,?,?,?);");
            ps.setString(1, "Dupont");
            ps.setString(2, "Toto");
            ps.setString(3, "toto.dupont@email.com");
            ps.setString(4, "password");
            ps.setString(5, "EMPTOTO");
            ps.setBoolean(6, true);
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void should_return_True_when_user_is_in_database(){
        User user = new User("toto.dupont@email.com" , "password");
        boolean testedValue;
        //tested method
        testedValue = LoginDAO.submit(user);
        assertEquals(true , testedValue);
    }

    @Test
    public void should_return_false_when_login_is_not_in_database(){
        User user = new User("toto" , "password");
        boolean testedValue;

        //tested method
        testedValue = LoginDAO.submit(user);
        assertEquals(false , testedValue);
    }
    @Test
    public void should_return_false_when_password_is_wrong_but_login_is_right(){
        User user = new User("toto.dupont@email.com" , "mdp");
        boolean testedValue;

        //tested method
        testedValue = LoginDAO.submit(user);
        assertEquals(false , testedValue);
    }

    @AfterAll
    public static void after_tests() {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("delete from user where login=?");
            ps.setString(1, "toto.dupont@email.com");
            ps.execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
