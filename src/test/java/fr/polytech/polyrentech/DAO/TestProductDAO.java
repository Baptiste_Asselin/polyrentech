package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.Material;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
public class TestProductDAO {
    @Test
    void getMaterial_should_return_the_right_material(){
        Material material = new Material("getMaterial","V1","MATES","h","Test getMaterial");
        MaterialDAO.delete(material);
        int idMaterial = MaterialDAO.save(material);
        //tested method
        Material resMaterial = ProductDAO.getMaterial(idMaterial);
        MaterialDAO.delete(material);
        assertEquals(material.getName(),resMaterial.getName());
        assertEquals(material.getImageURL(),resMaterial.getImageURL());

    }
    @Test
    public void getMaterial_should_throw_Illegal_Argument_Exception_when_idMaterial_not_in_database(){
        assertThrows(IllegalArgumentException.class , () -> ProductDAO.getMaterial(0),"doesn't throw exception when idMaterial is not in database");
    }
}
