package fr.polytech.polyrentech;

import fr.polytech.polyrentech.view.LoginView;
import fr.polytech.polyrentech.view.MainView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        MainView mv = new MainView();
        Scene scene = new Scene(mv, 900, 600);
        stage.setTitle("Polyrentech");
        stage.setScene(scene);
        mv.setStage(stage);
        stage.show();
    }
}