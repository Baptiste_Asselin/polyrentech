package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.Material;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MaterialDAO {
    /*
    *@param material Le materiel a ajouter a la base de donnée
    * @return int retourne l'idMaterial du material ajouté, si il y a eu un problème retourne -1
    * @throws IllegalArgumentException cette exception est renvoyé quand le material existe deja dans la base de donnée avec la meme reference.
     */
    public static int save(Material material) {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("Select reference from material where reference = ?");
            ps.setString(1 , material.getReference());
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                throw new IllegalArgumentException("Ce materiel existe deja avec la meme reference dans la base de donnée");
            }
            else{
                ps = con.prepareStatement("insert into material(name , version , reference , imageURL , materialInformation) values (?,?,?,?,?);");
                ps.setString(1 , material.getName());
                ps.setString(2 , material.getVersion());
                ps.setString(3 ,material.getReference());
                ps.setString(4 , material.getImageURL());
                ps.setString(5 , material.getMaterialInformation());
                if (ps.executeUpdate() >= 1){
                    return getIdMaterialFromReference(material.getReference());
                }else{
                    return -1;
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }
    /*
    *@param material le material a retirer de la abse de donnée.La référence est utilisé
     */
    public static void delete(Material material) {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("delete from material where reference = ? ;");
            ps.setString(1, material.getReference());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
    *@param research : une String contenant la recherche.
    * @return List<Integer> retourne une liste contenant tout les idsMaterial correspondant a la recherche
     */
    public static List<Integer> getIdMaterial(String research){
        List<Integer> idMaterials = new LinkedList<Integer>();
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("Select distinct idmaterial from material where name like ? or version like ? or reference like ? or imageURL like ? or materialInformation like ?; ");
            ps.setString(1,'%'+research+'%');
            ps.setString(2,'%'+research+'%');
            ps.setString(3,'%'+research+'%');
            ps.setString(4,'%'+research+'%');
            ps.setString(5,'%'+research+'%');
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                idMaterials.add(rs.getInt(1));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return idMaterials;
    }
    /*
    *@param reference : la reference a utiliser pour obtenir un idMaterial
    * @return int : l'idMaterial correspondant a la reference
     */
    public static int getIdMaterialFromReference(String reference){
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("Select idMaterial from material where reference = ?;");
            ps.setString(1,reference);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }else{
                return 0;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }
}
