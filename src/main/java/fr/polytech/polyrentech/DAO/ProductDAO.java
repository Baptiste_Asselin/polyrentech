package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.Material;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ProductDAO {
    /*
    *@param idMaterial un entier correspondant a l'id d'un materiel de la base de donnée
    *@return Material retourne un material avec le nom et l'url de l'image obtenu dans la base de donnée;
    * @throws IllegalArgumentException cette exception est retourné si l'idMaterial n'existe pas dans la base de donnée
     */
    public static Material getMaterial(int idMaterial) {
        Material material = null;
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("select name , imageURL from material where idMaterial = ? ;");
            ps.setInt(1 , idMaterial);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                material = new Material(idMaterial, rs.getString(1), rs.getString(2));
            }else{
                throw  new IllegalArgumentException("L'id materiel n,'existe pas dans la base de donnée");
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return material;
    }
    /*
    *@return List<Integer> : une liste contenant tout les idMaterials
     */
    public static List<Integer> getAllMaterialId() {
        List<Integer> res = new LinkedList<>();
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech", "admin", "admin")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("select idMaterial from material;");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                res.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}