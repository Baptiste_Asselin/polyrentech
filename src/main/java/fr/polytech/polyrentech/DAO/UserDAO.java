package fr.polytech.polyrentech.DAO;

import fr.polytech.polyrentech.Model.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO {

    public static User getUser(String login) {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("Select * from user where login = ?;");
            ps.setString(1 , login);
            ResultSet rs = ps.executeQuery();
            boolean res = rs.next();
            if(res) {
                User user = new User(rs.getInt(1), rs.getString(4), rs.getString(7), rs.getBoolean(6), rs.getString(2), rs.getString(3));
                return user;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
