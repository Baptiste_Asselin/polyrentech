package fr.polytech.polyrentech.DAO;
import java.sql.*;

import fr.polytech.polyrentech.Model.User;

public class LoginDAO {
    /*
    *@param user L'utilisateur pour lequel on doit verifier le login et le mot de passe
    * @return boolean un boolean est retourné si le login et le password sont trouvés dans la base de donné
    *
     */
    public static boolean submit(User user) {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/polyrentech","admin","admin")){
            Class.forName("com.mysql.cj.jdbc.Driver");
            PreparedStatement ps = con.prepareStatement("Select * from user where login = ?  and password = ?;");
            ps.setString(1 , user.getLogin());
            ps.setString(2 , user.getPassword());
            ResultSet rs = ps.executeQuery();
            boolean res = rs.next();
            return res;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
