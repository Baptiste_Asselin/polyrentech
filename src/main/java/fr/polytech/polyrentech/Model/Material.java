package fr.polytech.polyrentech.Model;
public class Material {
    private int idMaterial;
    private String name;
    private String version;
    private String reference;
    private String imageURL;
    private String materialInformation;

    public Material(int idMaterial, String name, String version, String reference, String imageURL, String materialInformation) {
        this.idMaterial = idMaterial;
        this.name = name;
        this.version = version;
        this.reference = reference;
        this.imageURL = imageURL;
        this.materialInformation = materialInformation;
    }

    public Material(int idMaterial, String name, String imageURL) {
        this.idMaterial = idMaterial;
        this.name = name;
        this.imageURL = imageURL;
    }

    public Material(String name, String version, String reference, String imageURL, String materialInformation) {
        this.name = name;
        this.version = version;
        this.reference = reference;
        this.imageURL = imageURL;
        this.materialInformation = materialInformation;
    }

    public int getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(int idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaterialInformation() {
        return materialInformation;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setMaterialInformation(String materialInformation) {
        this.materialInformation = materialInformation;
    }
}
