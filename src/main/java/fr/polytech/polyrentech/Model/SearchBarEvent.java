package fr.polytech.polyrentech.Model;

import javafx.event.Event;
import javafx.event.EventType;

import java.util.List;

public class SearchBarEvent extends Event {

    public static EventType<SearchBarEvent> EVENT_TYPE_SEARCH_BAR = new EventType<>(EventType.ROOT, "search_bar_event");

    private final List<Integer> ids;

    public SearchBarEvent(EventType<? extends Event> eventType, List<Integer> ids) {
        super(eventType);
        this.ids = ids;
    }

    public List<Integer> getIds() {
        return ids;
    }
}
