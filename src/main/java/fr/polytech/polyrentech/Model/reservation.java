package fr.polytech.polyrentech.Model;

import java.util.Date;

public class reservation {
    private Date startDate;
    private Date endDate;
    private int idUser;
    private int idMaterial;

    public reservation(Date startDate, Date endDate, int idUser, int idMaterial) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.idUser = idUser;
        this.idMaterial = idMaterial;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(int idMaterial) {
        this.idMaterial = idMaterial;
    }
}
