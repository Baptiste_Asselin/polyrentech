package fr.polytech.polyrentech.Model;

public class User {
    private int idUser;
    private String login;
    private String password;
    private Boolean admin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    private String name;

    private String surname;

    public User(int idUser, String login, String password, Boolean admin, String n, String sn) {
        this.idUser = idUser;
        this.login = login;
        this.password = password;
        this.admin = admin;
        name = n;
        surname = sn;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

}
