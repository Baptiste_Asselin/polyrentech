package fr.polytech.polyrentech.Model;

/**
 * Cette classe représente une session très simplifié qui permet de savoir si l'utilisateur est connecté et si c'est un
 * administrateur.
 *
 * @author Baptiste
 */
public class UserSession {
    private static UserSession session;

    private boolean connected;

    private User user;

    /**
     * permet de récupérer l'utilisateur connecté, sinon retourne une exception
     * @return l'utilisateur
     * @throws IllegalStateException si l'utilisateur n'est pas connecté
     */
    public User getUser() {
        return user;
    }

    /**
     * permet de définir l'utilisateur, si l'utilisateur n'est pas connecté, passe l'utilisateur à connecté
     * @param usr l'utilisateur
     */
    public void setUser(User usr) {
        if(user == null) {
            connected = true;
        } else if (usr == null) {
            connected = false;
        }
        user = usr;
    }

    /**
     * permet d'obtenir l'instance de la sesssion (singleton)
     * @return l'instance de la session
     */
    public static UserSession getInstance() {
        if(session == null) {
            session = new UserSession();
        }
        return session;
    }

    private UserSession() {
        connected = false;
        user = null;
    }

    /**
     * permet de savoir si l'utilisateur est connecté
     * @return vrai si l'utilisateur est connecté, faux sinon
     */
    public boolean isConnected() {
        return connected;
    }
}
