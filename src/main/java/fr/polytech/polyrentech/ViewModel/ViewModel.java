package fr.polytech.polyrentech.ViewModel;

import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Représente un viewmodel de base, il permet au viewmodel qui l'hérite de pouvoir récupérer la référence au stage
 * (fenêtre de l'appli) et de pouvoir ainsi changer de scene et donc de vue.
 */
public abstract class ViewModel {

    private Stage stage;

    /**
     * permet de spécifier le stage (fenêtre) de l'application
     * @param stg le stage de l'application
     */
    public void setStage(Stage stg) {
        stage = stg;
    }

    /**
     * permet de récupérer le stage (fenêtre) de l'application
     * @return le stage de l'application
     */
    public Stage getStage() {
        return stage;
    }

}
