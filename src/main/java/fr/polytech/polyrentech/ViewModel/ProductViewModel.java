package fr.polytech.polyrentech.ViewModel;

import fr.polytech.polyrentech.DAO.ProductDAO;
import fr.polytech.polyrentech.Model.Material;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.LinkedList;
import java.util.List;

public class ProductViewModel {
    private final StringProperty name = new SimpleStringProperty("");
    private final IntegerProperty idMaterial = new SimpleIntegerProperty();
    private final StringProperty imageURL = new SimpleStringProperty((""));

    public StringProperty name(){return name;}
    public IntegerProperty idMaterial(){return idMaterial;}
    public StringProperty imageURL(){return imageURL ;}

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getIdMaterial() {
        return idMaterial.get();
    }

    public IntegerProperty idMaterialProperty() {
        return idMaterial;
    }

    public void setIdMaterial(int idMaterial) {
        this.idMaterial.set(idMaterial);
    }

    public String getImageURL() {
        return imageURL.get();
    }

    public StringProperty imageURLProperty() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL.set(imageURL);
    }

    /*
    * Permet d'obtenir le nom et l'url de l'image grace a un idMaterial
    * @throws IllegalStateException cette exception est renvoyé si l'objet n'a pas d'idMaterial d'initialisé
     */
    public void getMaterial(){
        if(idMaterial == null){
            throw new IllegalStateException("Il n'y a pas d'idMaterial");
        }
        Material material = ProductDAO.getMaterial(idMaterial.get());
        setName(material.getName());
        setImageURL(material.getImageURL());
    }
}