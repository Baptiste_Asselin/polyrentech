package fr.polytech.polyrentech.ViewModel;

import fr.polytech.polyrentech.DAO.ProductDAO;
import fr.polytech.polyrentech.Model.SearchBarEvent;
import fr.polytech.polyrentech.Model.UserSession;
import fr.polytech.polyrentech.view.LoginView;
import fr.polytech.polyrentech.view.MainView;
import fr.polytech.polyrentech.view.ProductView;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;

/**
 * Représente le viewmodel pour la vue principale, il permet de gérer la récupération des produits à afficher suivant
 * le résultat de la barre de recherche
 *
 * @author Baptiste
 */
public class MainViewModel extends ViewModel implements EventHandler<SearchBarEvent> {
    private ObservableList<Node> items;

    private StringProperty name;
    private StringProperty surname;

    /**
     * Constructeur par défaut, charge tous les produits depuis la base de donnée qui seront alors automatiquement bind
     * à la vue
     */
    public MainViewModel() {
        items = FXCollections.observableArrayList();
        name = new SimpleStringProperty();
        surname = new SimpleStringProperty();
        List<Integer> materialIds = ProductDAO.getAllMaterialId();
        for(int id : materialIds) {
            ProductView pv = new ProductView(id);
            items.add(pv);
        }
        if(UserSession.getInstance().isConnected()) {
            name.set(UserSession.getInstance().getUser().getName());
            surname.set(UserSession.getInstance().getUser().getSurname());
        }
    }

    public StringProperty name() {
        return name;
    }

    public StringProperty surname() {
        return surname;
    }

    /**
     * permet de récupérer la liste des produits actuellement dans la vue (via un bind avec la vue)
     * @return la liste des produits affichés dans la vue
     */
    public ObservableList<Node> items() {
        return items;
    }

    /**
     * Appelé automatiquement lorsque la barre de recherche envoie un évènement lorsuqe l'utilisateur valide le mot clé.
     * Cette méthode vide la liste des produits et la rempli avec les produits correspondant à la recherche.
     * @param event l'évènement de recherche (contient la liste des ids des produits qui correspondent à la recherche)
     */
    @Override
    public void handle(SearchBarEvent event) {
        items.clear();
        for(int id : event.getIds()) {
            ProductView pv = new ProductView(id);
            items.add(pv);
        }
    }

    /**
     * Affiche la page de connection (LoginView) si l'utilisateur n'est pas encore connecté
     */
    public void showLogin() {
        LoginView lv = new LoginView();
        getStage().setScene(new Scene(lv, 900, 600));
        lv.setStage(getStage());
    }
}
