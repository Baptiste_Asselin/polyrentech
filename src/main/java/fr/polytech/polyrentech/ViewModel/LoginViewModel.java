package fr.polytech.polyrentech.ViewModel;
import fr.polytech.polyrentech.Converter.LoginConverter;
import fr.polytech.polyrentech.DAO.LoginDAO;
import fr.polytech.polyrentech.DAO.UserDAO;
import fr.polytech.polyrentech.Model.User;
import fr.polytech.polyrentech.Model.UserSession;
import fr.polytech.polyrentech.view.MainView;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;

public class LoginViewModel extends ViewModel{
    private final StringProperty login = new SimpleStringProperty("");
    private final StringProperty password = new SimpleStringProperty("") ;
    private final BooleanProperty showErrorMessage = new SimpleBooleanProperty(false);
    private final LoginConverter converter = new LoginConverter();
    public StringProperty login(){
        return login;
    }
    public StringProperty password(){
        return password;
    }
    public String getLogin() {
        return login.get();
    }

    public String  getPassword(){
        return password.get();
    }
    public BooleanProperty showErrorMessage(){
        return showErrorMessage;
    }
    public void setShowErrorMessage(boolean value){
        showErrorMessage.set(value);
    }
    /*
    *Permet de vérifier si l'utilisateur est dans la base de donnée afin de connecter l'utilisateur
    *@exception IllegalStateException cette exception est retourne si les attributs login et/ou password sont vide
     */
    public void submit(){
        if(this.getPassword().isBlank() || this.getLogin().isBlank()){
            throw new IllegalStateException("Les champs login et password sont vide");
        }
        User data = converter.toUser(this);
        if (!LoginDAO.submit(data)){
            setShowErrorMessage(true);
        } else {
            UserSession.getInstance().setUser(UserDAO.getUser(login.get()));
            getStage().setScene(new Scene(new MainView(), 900, 600));
        }
    }
}
