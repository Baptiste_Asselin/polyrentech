package fr.polytech.polyrentech.ViewModel;

import fr.polytech.polyrentech.Converter.MaterialConverter;
import fr.polytech.polyrentech.DAO.MaterialDAO;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MaterialViewModel {
    private final StringProperty name = new SimpleStringProperty("");
    private final StringProperty version = new SimpleStringProperty("");
    private final StringProperty reference = new SimpleStringProperty("");
    private final StringProperty imageURL = new SimpleStringProperty("");
    private final StringProperty materialInformation = new SimpleStringProperty("");

    public StringProperty name(){return name;}

    public StringProperty version(){return version;}

    public StringProperty reference(){return reference;}

    public StringProperty imageURL(){return imageURL;}

    public StringProperty materialInformation(){return materialInformation;}

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getVersion() {
        return version.get();
    }

    public StringProperty versionProperty() {
        return version;
    }

    public void setVersion(String version) {
        this.version.set(version);
    }

    public String getReference() {
        return reference.get();
    }

    public StringProperty referenceProperty() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference.set(reference);
    }

    public String getImageURL() {
        return imageURL.get();
    }

    public StringProperty imageURLProperty() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL.set(imageURL);
    }

    public String getMaterialInformation() {
        return materialInformation.get();
    }

    public StringProperty materialInformationProperty() {
        return materialInformation;
    }

    public void setMaterialInformation(String materialInformation) {
        this.materialInformation.set(materialInformation);
    }
    public boolean save(){
        if(MaterialDAO.save(MaterialConverter.toMaterial(this)) == -1){
            return false;
        }else{
            return true;
        }
    }
}
