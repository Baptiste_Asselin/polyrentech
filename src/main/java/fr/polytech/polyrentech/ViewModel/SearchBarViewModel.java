package fr.polytech.polyrentech.ViewModel;

import fr.polytech.polyrentech.DAO.MaterialDAO;
import fr.polytech.polyrentech.Model.SearchBarEvent;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.Event;
import javafx.event.EventHandler;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchBarViewModel {

    private Set<EventHandler<SearchBarEvent>> listeners;
    private final StringProperty research = new SimpleStringProperty("");

    public String getResearch() {
        return research.get();
    }

    public StringProperty research() {
        return research;
    }

    public SearchBarViewModel() {
        listeners = new HashSet<>();
    }

    public void setResearch(String research) {
        this.research.set(research);
    }
    public List<Integer> submit(){
        List<Integer> res = MaterialDAO.getIdMaterial(research.get());
        for(EventHandler<SearchBarEvent> eventHandler : listeners) {
            eventHandler.handle(new SearchBarEvent(SearchBarEvent.EVENT_TYPE_SEARCH_BAR, res));
        }
        return res;
    }

    public void addListener(EventHandler<SearchBarEvent> eventHandler) {
        listeners.add(eventHandler);
    }
}
