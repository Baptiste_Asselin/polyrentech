package fr.polytech.polyrentech.Converter;

import fr.polytech.polyrentech.Model.Material;
import fr.polytech.polyrentech.ViewModel.ProductViewModel;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

public class ProductConverter {
    public Material toMaterial(StringProperty name , StringProperty imageURL , IntegerProperty idMaterial){return new Material(idMaterial.get() , name.get() , imageURL.get());}
}
