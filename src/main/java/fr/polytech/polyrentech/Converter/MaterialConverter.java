package fr.polytech.polyrentech.Converter;

import fr.polytech.polyrentech.Model.Material;
import fr.polytech.polyrentech.ViewModel.MaterialViewModel;

public class MaterialConverter {
    public static Material toMaterial(MaterialViewModel materialVM){ return new Material(materialVM.getName(), materialVM.getVersion(), materialVM.getReference() , materialVM.getImageURL() , materialVM.getMaterialInformation());}
}
