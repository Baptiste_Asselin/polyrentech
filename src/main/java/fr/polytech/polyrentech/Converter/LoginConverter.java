package fr.polytech.polyrentech.Converter;
import fr.polytech.polyrentech.Model.User;
import fr.polytech.polyrentech.ViewModel.LoginViewModel;

public class LoginConverter {
    /*
    *@param ViewModel le LoginViewModel a convertir en User
    * @return User un utilisateur contenant les mêmes informations que le LoginViewModel en parametre est retourné
     */
    public User toUser(LoginViewModel viewModel){
        if(viewModel.getLogin().isBlank() || viewModel.getPassword().isBlank()) {
            throw new IllegalArgumentException("login and password must not be blank");
        }
        return new User(viewModel.getLogin() , viewModel.getPassword());
    }
}
