package fr.polytech.polyrentech.view;

import fr.polytech.polyrentech.ViewModel.ProductViewModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import java.io.IOException;

/**
 * Cette classe représente la carte qui représente un appareil dans la liste des appareils
 *
 * @author Hugo LIN, Baptiste ASSELIN
 */
public class ProductView extends VBox{

    @FXML
    private Label lProduct;
    @FXML
    private ImageView ivImageProduct;
    @FXML
    private Button bDetails;

    private int idMaterial;

    private ProductViewModel viewModel;

    /**
     * Constructeur par défaut, load la vue FXMl et bind le "controller"
     */
    public ProductView(int id) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ProductView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        idMaterial = id;
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * methode appelé automatiquement après le chargement de la vue, créé et bind la vue avec le vue-model
     */
    public void initialize() {
        viewModel = new ProductViewModel();
        lProduct.textProperty().bind(viewModel.nameProperty());
        viewModel.setIdMaterial(idMaterial);
        viewModel.getMaterial();
    }
}
