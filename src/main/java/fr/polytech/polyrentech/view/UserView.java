package fr.polytech.polyrentech.view;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Cette classe représente l'interface de la liste de tous les utilisateurs de l'application
 */
public class UserView {
    public UserView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserView.fxml"));
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
