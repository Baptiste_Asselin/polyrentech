package fr.polytech.polyrentech.view;

import fr.polytech.polyrentech.ViewModel.SearchBarViewModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import java.io.IOException;

/**
 * Cette classe représente la barre de recherche
 *
 * @author Hugo LIN
 */
public class SearchBarView extends HBox{

    private SearchBarViewModel viewModel;

    @FXML
    private TextField tfSearch;

    /**
     * Constructeur par défaut, load la vue FXMl et bind le "controller"
     */
    public SearchBarView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("SearchBarView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void initialize() {
        viewModel = new SearchBarViewModel();
        viewModel.research().bindBidirectional(tfSearch.textProperty());
        tfSearch.setOnAction((event -> {
            viewModel.submit();
        }));
    }

    public SearchBarViewModel getViewModel() {
        return viewModel;
    }
}
