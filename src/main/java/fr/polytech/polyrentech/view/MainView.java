package fr.polytech.polyrentech.view;

import fr.polytech.polyrentech.Model.UserSession;
import fr.polytech.polyrentech.ViewModel.MainViewModel;
import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.w3c.dom.events.MouseEvent;

import java.io.IOException;
import java.util.Objects;

/**
 * Représente la vue principale de l'application, elle contient une bar de recherche, un bouton de connection et
 * une "liste" des différents produits disponible.
 *
 * @author Baptiste
 */
public class MainView extends AnchorPane {

    @FXML
    private Label name;
    @FXML
    private Label surname;
    @FXML
    private ImageView image;
    @FXML
    private FlowPane container;

    @FXML
    private SearchBarView searchBar;

    private MainViewModel viewModel;

    /**
     * Constructeur par défaut, charge la vue FXML et appel l'initialisation (via la methode load() du FXMLLoader)
     */
    public MainView() {
        FXMLLoader loader = new FXMLLoader(LoginView.class.getResource("MainView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Cette méthode est automatiquement appelé à la fin du chargement de la vue FXML.
     * L'image du bouton de connection est appliqué à la vue et la liste des produits disponibles est appliqué à la vue
     * via la création et le binding du viewmodel associé
     */
    public void initialize() {
        try {
            Image img = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/fr/polytech/polyrentech/assets/login.png")));
            image.setImage(img);
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewModel = new MainViewModel();
        container.getChildren().setAll(viewModel.items());
        Bindings.bindContent(container.getChildren(), viewModel.items());
        searchBar.getViewModel().addListener(viewModel);
        name.textProperty().bind(viewModel.name());
        surname.textProperty().bind(viewModel.surname());

        viewModel.setStage(viewModel.getStage());
    }

    public void login(Event event) {
        if(!UserSession.getInstance().isConnected()) {
            viewModel.showLogin();
        }
    }

    /**
     * set the stage for the viewmodel
     * @param stage the stage (window)
     */
    public void setStage(Stage stage) {
        viewModel.setStage(stage);
    }
}
