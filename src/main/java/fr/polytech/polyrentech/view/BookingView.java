package fr.polytech.polyrentech.view;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Cette classe représente l'interface de location d'un appareil
 *
 * @author Hugo LIN
 */
public class BookingView {

    /**
     * constructeur par défaut, load la vue FXMl et bind le "controller"
     */
    public BookingView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("BookingView.fxml"));
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
