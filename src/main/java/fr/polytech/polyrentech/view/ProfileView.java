package fr.polytech.polyrentech.view;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Cette classe représente l'interface qui montre le détails des profils de chaque utilisateur
 */
public class ProfileView {
    public ProfileView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfileView.fxml"));
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
