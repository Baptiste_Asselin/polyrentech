package fr.polytech.polyrentech.view;

import fr.polytech.polyrentech.ViewModel.LoginViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Cette classe représente l'interface de login de l'application
 *
 * @author Hugo LIN, Baptiste ASSELIN
 */
public class LoginView extends VBox {

    @FXML
    private Label lbErrorMessage;
    @FXML
    private TextField tfUsername;
    @FXML
    private PasswordField pfPassword;

    private LoginViewModel viewModel;

    /**
     * constructeur par défaut, load la vue FXMl et bind le "controller"
    */
    public LoginView() {
        FXMLLoader loader = new FXMLLoader(LoginView.class.getResource("LoginView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * methode appelé automatiquement après le chargement de la vue, créé et bind la vue avec le vue-model
     */
    public void initialize() {
        viewModel = new LoginViewModel();
        tfUsername.textProperty().bindBidirectional(viewModel.login());
        pfPassword.textProperty().bindBidirectional(viewModel.password());
        lbErrorMessage.visibleProperty().bindBidirectional(viewModel.showErrorMessage());
    }

    /**
     * listener du bouton  devalidation du formulaire de login, appél le vue-model pour la vérification du mot de passe pour la connexion
     * @param event evenement du bouton
     */
    public void submit(ActionEvent event) {
        if(tfUsername.getText().isBlank() || pfPassword.getText().isBlank()) {
            return;
        }
        viewModel.submit();
    }

    /**
     * set the stage for the viewmodel
     * @param stage the stage (window)
     */
    public void setStage(Stage stage) {
        viewModel.setStage(stage);
    }
}
