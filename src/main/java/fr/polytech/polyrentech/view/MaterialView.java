package fr.polytech.polyrentech.view;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Cette classe représente l'interface le details de chaque materiel
 */
public class MaterialView {
    public MaterialView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MaterialView.fxml"));
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
