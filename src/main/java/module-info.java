module fr.polytech.polyrentech {
    requires javafx.controls;
    requires javafx.fxml;

    requires net.synedra.validatorfx;
    requires java.sql;

    opens fr.polytech.polyrentech.view to javafx.fxml;
    exports fr.polytech.polyrentech;
}